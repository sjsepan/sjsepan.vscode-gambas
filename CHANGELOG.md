# Change Log

## [v0.0.7]

- renamed display name in pkg json because MS Marketplace said it conflicts (w/ itself as it turns out).

## [v0.0.6]

- canonical layout reorg

## [v0.0.5]

- still correcting naming-errors in manifest

## [v0.0.4]

- simplify naming to resolve naming-errors

## [v0.0.3]

- bumped version, to supersede version error

## [v0.0.2]

- skipped, due to version error

## [v0.0.1]

- Initial Syntax Highlight and Snippets support
