#scratchpad

# install Node Package Manager

sudo apt install npm

# install vsce

npm install -g @vscode/vsce

# package (see https://gitlab.com/sjsepan/repackage-vsix)

vsce-package-vsix.sh

# publish

vsce publish -i ./vscode-gambas-0.0.7.vsix
npx ovsx publish vscode-gambas-0.0.7.vsix --debug --pat <PAT>