# Gambas3 Syntax Highlight and Snippet Support

## About

This Visual Studio Code extension is a fork of [**@microhobby's extension**](https://marketplace.visualstudio.com/items?itemName=microhobby.gambas3) supporting [**Gambas 3**](https://gambaswiki.org/wiki/doc/whatisgambas?nh) language.

The layout of the extension was re-organized to match what I've done with other extensions, an extension.vsixmanifest was added, and a few more keys added to the package.json.

The 'heavy lifting' was already done by @MicroHobby in the form of syntax-highlighting; snippets were added, and also some language pairs elements that were not already included in language-configuration.json.

Since there appears to be a hint of support for [**Gambas Scripting**](http://gambaswiki.org/wiki/doc/scripting) in gambas3.tmLanguage.json, the .gbs extension was also added to package.json and extension.vsixmanifest.

## Features

- Syntax highlighting
    (e.g. `For`, `For Each`, `While`, `Select...Case`, `If...Else If...Else`, `Do...While..Until`)
- Code snippets
    (e.g. `For`, `For Each`, `While`, `Select...Case`, `If...Else If...Else`, `Do...While..Until`)

## Installation

### From repository

Download the .vsix file and choose 'Install from VSIX...' from the app.

## Changelog

See the [changelog](https://gitlab.com/sjsepan/vscode-gambas/blob/HEAD/CHANGELOG.md) for details.

## Issues

-

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/15/2025
